/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zegar;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 *
 * @author Łukasz Małek
 */
public class Zegar extends Thread
{
	private  int sek=0;
	private  int min=0;
	private  int godz=0;
	private JLabel czas;

        
	public  Zegar(int godz, int min, int sek, JLabel czas)
	{
            this.godz=godz;
            this.min=min;
            this.sek=sek;
            this.czas=czas;
	}
	
	public int getsek(){return sek;}
	public int getmin(){return min;}
	public int getgodz(){return godz;}
	
	public void setsek(int s){this.sek=s;}
	public void setmin(int m){this.min=m;}
	public void setgodz(int g){this.godz=g;}
	
	public void ustaw()
	{
		
		if(sek>=60){
			min+=sek/60;
			sek=sek%60;
		}
		if(min>=60){
			godz+=min/60;
			min=min%60;
		}
		if(godz>=24){
			godz=0;
		}
				
	}
	
	public String wypisz()
	{
		return getgodz() +":"+getmin()+":"+getsek();
	}
	
	
	
	public void tykniecie()
	{
		
		this.sek=this.getsek()+1;
		this.min=this.getmin();
		this.godz=this.getgodz();
		ustaw();
		
	}
		
	public void run()
	{
        ustaw();
       
		try
                {
                   for(;;)
                   {

                        ustaw();
                        czas.setText(wypisz());
                        czas.paintImmediately(0, 0, czas.getSize().width,czas.getSize().height); 
                        Thread.sleep(1000);
                        tykniecie();   
                          
                   }
               }
		 catch (InterruptedException ex) 
               {
                   Logger.getLogger(Zegar.class.getName()).log(Level.SEVERE, null, ex);
               }
		
	}
}
