/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etykieta;

import java.awt.Color;
import javax.swing.JLabel;

/**
 *
 * @author Łukasz Małek
 */
public class Etykieta extends JLabel implements Runnable
{
       private Color c2=Color.BLUE;
    private Color c1=Color.GREEN;
 
    private int delay=1000;
    private JLabel label;

    public Etykieta(JLabel l, Color k1, Color k2,int d)
    { 
        
        label=l;
        c1=k1;
       
        
        
        
        
        c2=k2;
        delay=d;
        label.setOpaque(true);
        
    }
   
  
    public void zmienKolor(Color c)
    {
        label.setBackground(c);
    }
    public Color getKolor()
    {
        return getBackground();
    }
     public void tekst(Color c)
    {
        label.setForeground(c);
    }
     
     public void run()
	{
             try
              {
               
                while(true){
                    zmienKolor(c1); 
                    label.paintImmediately(0, 0, label.getSize().width,label.getSize().height);
                    Thread.sleep( delay);
                    zmienKolor(c2);
                    label.paintImmediately(0, 0, label.getSize().width,label.getSize().height);
                     
                    Thread.sleep( delay);
                 }
                              
               
              }
                catch(InterruptedException e ) 
                {
                 Thread.currentThread().interrupt();
                }
                
        }
}   

