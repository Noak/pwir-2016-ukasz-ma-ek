
public class Zad3
{
	
	public static void main (String args[]) 
	{
		Zegar z=new Zegar(10,12,44);
		Zegar.nastaw(z);
		System.out.println(Zegar.wypisz(z));
		
		Zegar z2=new Zegar(15,65,3);
		Zegar.nastaw(z2);
		System.out.println(Zegar.wypisz(z2));
		
		Zegar z3=new Zegar(15,14,65);
		Zegar.nastaw(z3);
		System.out.println(Zegar.wypisz(z3));
		
		Zegar z4=new Zegar(34,12,54);
		Zegar.nastaw(z4);
		System.out.println(Zegar.wypisz(z4));
		
		Zegar z5=new Zegar(12,33,4);
		Zegar.nastaw(z5);
		Zegar.format(z5,12);
		System.out.println(Zegar.wypisz(z5));
		Zegar.format(z5,24);
		System.out.println(Zegar.wypisz(z5));
		
		System.out.println("tyknięcie");
		Zegar z6=new Zegar(14,12,59);
		Zegar.nastaw(z6);
		System.out.println(Zegar.wypisz(z6));
		Zegar.tykniecie(z6);
		System.out.println(Zegar.wypisz(z6));
		
		System.out.println("minelo 5 sekund");
		Zegar z7=new Zegar(14,12,59);
		Zegar.ustaw(z7);
		Zegar.new5sec(z7);
		
	}
}

class Zegar extends Thread
{
	private static int sek=0;
	private static int min=0;
	private static int godz=0;
	
	Zegar(int g, int m, int s)
	{
		this.s=s;
		this.m=m;
		this.g=g;
	}
	
	public int getsek(){return s;}
	public int getmin(){return m;}
	public int getgodz(){return g;}
	
	public void setsek(int s){this.s=s;}
	public void setmin(int m){this.m=m;}
	public void setgodz(int g){this.g=g;}
	
	public static Zegar ustaw(Zegar zegarek)
	{
		int sek=0;
		int min=0;
		int godz=0;
		
		sek=zegarek.getsek();
		min=zegarek.getmin();
		godz=zegarek.getgodz();
		
		if(sek>=60){
			min+=sek/60;
			sek=sek%60;
		}
		if(min>=60){
			godz+=min/60;
			min=min%60;
		}
		if(godz>=24){
			godz=0;
		}
				
		Zegar z=new Zegar(godz, min, sek);
		
	return z;
	}
	
	public static String wypisz(Zegar zegarek){
		return zegarek.getgodz() +":"+zegarek.getmin()+":"+zegarek.getsek();
	}
	
	public static Zegar format(Zegar zegarek, int f)
	{
		int sek=0;
		int min=0;
		int godz=0;
		sek=zegarek.getsek();
		min=zegarek.getmin();
		godz=zegarek.getgodz();
		
		if(f==12){
		if(godz>12){
			godz=godz-12;
		}
		}
		
		if(f==24){
		if(godz<12){
			godz=godz+12;
		}
		}
		
		Zegar z = new Zegar(godz, min, sek);
		return z;
	}
	
	public static Zegar tykniecie(Zegar zegarek)
	{
		int sek=0;
		int min=0;
		int godz=0;
		sek=zegarek.getsek()+1;
		min=zegarek.getmin();
		godz=zegarek.getgodz();
		Zegar zeg = new Zegar(godz, min, sek);
		nastaw(zeg);
		
		return zeg;
	}
		
	public static void new5sec(Zegar zegarek)
	{
		try
		{
			for (int i=0; i<5; i++) {
				System.out.println(wypisz(zegarek));
				sleep(1000);
				tykniecie(zegarek);
			}
		}
		catch(InterruptedException e ) 
		{
			return;
		}
		
	}
}

